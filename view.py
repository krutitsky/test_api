from flask import (
	render_template,
	request,
	redirect,
	current_app
)
from app import app
import hashlib
import requests
import datetime
import os
import logging
import csv
import pandas as pd


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO, filename='log.log')
logging.getLogger('urllib3').setLevel('CRITICAL')
logging.getLogger('werkzeug').setLevel('CRITICAL')


if not os.path.exists('pay_info.csv'):
	with open('pay_info.csv', 'w', newline='') as f:
		csv.writer(f).writerow(['shop_order_id', 'amount', 'currency', 'description', 'time'])


def get_sign(*signs):
	sign = ':'.join(signs) + current_app.config['SECRET_KEY']
	return hashlib.sha256(sign.encode()).hexdigest()


@app.route('/', methods=['GET', 'POST'])
def index():

	df = pd.read_csv('pay_info.csv')
	if len(df) > 0:
		shop_order_id = str(int(df.iloc[[-1]]['shop_order_id']) + 1)
	else:
		shop_order_id = '1'

	if request.method == 'GET':
		name = 'Payment'
		return render_template('index.html', n=name)

	elif request.method == 'POST':

		amount = request.form['amount']
		currency = request.form['currency']
		description = request.form['description']

		logging.info('shop_order_id: {}; amount: {}; currency: {}; description: "{}"'.format(shop_order_id, amount, currency, description))

		df = pd.DataFrame([{'shop_order_id': shop_order_id,
						   'amount': amount,
						   'currency': currency,
						   'description': description,
						   'time': datetime.datetime.now()}])

		df.to_csv('pay_info.csv', mode='a', index=False, header=False)

		if currency == 'EUR':

			url = 'https://pay.piastrix.com/en/pay'
			method = 'POST'

			sign = get_sign(amount, current_app.config['CURRENCIES'][currency], current_app.config['SHOP_ID'], shop_order_id)

			params = {'amount': amount,
						'currency': current_app.config['CURRENCIES'][currency],
						'shop_id': current_app.config['SHOP_ID'],
					  	'sign': sign,
					  	'shop_order_id': shop_order_id,
						'description': description}

			return render_template('form.html', url=url, method=method, values=params)

		elif currency == 'USD':

			sign = get_sign(current_app.config['CURRENCIES'][currency], amount, current_app.config['CURRENCIES'][currency], current_app.config['SHOP_ID'], shop_order_id)

			params = {'payer_currency': current_app.config['CURRENCIES'][currency],
					'shop_amount': amount,
					'shop_currency': current_app.config['CURRENCIES'][currency],
					'shop_id': current_app.config['SHOP_ID'],
					'shop_order_id': shop_order_id,
					'sign': sign}

			if description:
				params['description'] = description

			url = 'https://core.piastrix.com/bill/create'

			response = requests.post(url, json=params).json()

			if response['error_code'] == 0:
				return redirect(response['data']['url'], code=301)

			error = response['message']
			logging.error('shop_order_id: {}; amount: {}; currency: {}; description: "{}"; error: {}'.format(shop_order_id, amount, currency, description, error))
			return 'error: {}'.format(error), 403

		elif currency == 'RUB':

			sign = get_sign(amount, current_app.config['CURRENCIES'][currency], 'payeer_rub', current_app.config['SHOP_ID'], shop_order_id)

			params = {'amount': amount,
					'currency': current_app.config['CURRENCIES'][currency],
					'payway': 'payeer_rub',
					'shop_id': current_app.config['SHOP_ID'],
					'shop_order_id': shop_order_id,
					'sign': sign}

			if description:
				params['description'] = description

			url = 'https://core.piastrix.com/invoice/create'

			response = requests.post(url, json=params).json()

			if response['error_code'] == 0:
				url = 'https://payeer.com/api/merchant/process.php'
				method = 'GET'

				values = {
						'm_curorderid': response['data']['data']['m_curorderid'],
						'm_historyid': response['data']['data']['m_historyid'],
						'm_historytm': response['data']['data']['m_historytm'],
						'referer': response['data']['data']['referer']
				}

				return render_template('form.html', url=url, method=method, values=values)

			error = response['message']
			logging.error('shop_order_id: {}; amount: {}; currency: {}; description: "{}"; error: {}'.format(shop_order_id, amount, currency, description, error))
			return 'error: {}'.format(error), 403


@app.route('/pay_info', methods=['GET'])
def pay_info():
	df = pd.read_csv('pay_info.csv')
	html_table = df.to_html(index=False, classes='table table-striped', border=0, na_rep='', justify='center').replace('<tr>', '<tr align="center">')

	return render_template('pay_info.html', html_table=html_table)


